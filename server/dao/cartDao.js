'use strict'

const 
    userModel = require('../models/userModel'),
    productModel = require('../models/productModel');

async function addProductToCart(productId, quantity, phone){

    userModel.findOneAndUpdate({phone : phone},{$push : 
    {
        cart : {
        quantity : quantity,
        productId : productId
    }}}).then((data) => {
        return true;
    }).catch((err) => {
        return false;
    })
}

async function getProductsInCart(phone){
    let userResponse = await userModel.findOne({phone : phone});
    let productsInCart = [];
   
    let cartObjects = userResponse.cart;
    for (const product in cartObjects){
        let productId = (parseInt(cartObjects[product].productId)).toString();
        let searchObj = {
            productId : productId
        }
        await productModel.findOne(searchObj).then((data) =>{
            productsInCart.push(data);
        }).catch(err => {
        });       
    };
   return productsInCart;
}

async function removeProductFromCart(phone, productId){
    await userModel.updateOne({phone : phone},{
        $pull : {cart : {
            productId : productId
        }}
    }).then((data)=>{
        return true;
    }).catch((err) =>{
        return false;
    })
}

async function randomHealthFunction(){
    return true;
}
module.exports = {
    addProductToCart : addProductToCart,
    getProductsInCart : getProductsInCart,
    removeProductFromCart : removeProductFromCart,
    randomHealthFunction : randomHealthFunction
}