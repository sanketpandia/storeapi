const
    productModel = require('../models/productModel');

async function addProduct(product){
    let produceToBeSaved = new productModel(product);
    produceToBeSaved.save();
    return true;
}

async function editProduct(product){
    console.log(await productModel.update({
        productId : product.productId
    },{
        name : product.name,
        quantity : product.quantity,
    price : product.price,
    category : product.category,
    imageLink : product.image,
    discount : product.discount
    }));
    return true;
}

async function deleteProduct(productId){
    
    await productModel.deleteOne({productId : productId}).then((data) =>
    {
        
        return true;
    }).catch(() =>{
        return false;
    });
   
}

module.exports = {
    addProduct : addProduct,
    editProduct : editProduct,
    deleteProduct : deleteProduct
}