'use strict'
const
    orderModel = require('../models/orderModel');

async function completeOrder(orderId){
    let updateValue = {
        pendingStatus : false
    }
    let searchValue = {
        id : orderId
    }
    await orderModel.findOneAndUpdate(searchValue,updateValue).then((data) =>{
        return true;
    }).catch((err) =>{
        return false;
    });
}

module.exports = {
    completeOrder : completeOrder
}