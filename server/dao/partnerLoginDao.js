'use strict'
const
    partnerModel = require('../models/partnerModel');

async function checkExistingUser(phone, otp, sessionId){
    let userFilter = { phone : phone}
    let updateValue = {
        otp: otp,
        isActive : true,
        sessionId : sessionId,
        phone : phone
    };
    
    // Running the find one and update query. If the query doesn't find anything, eit returns null. Hence the null check. And if null checked, new object is entered
    await userModel.findOneAndUpdate(userFilter, updateValue).then((data) => {
        if(data === null){
           return false;
            //currentUser.save()
        }
        return true;
    })
    .catch(err => {
       return false;
    });
    return true;
}

async function getUserDetails(phone, otp){
    let searchValue = { 
        phone : phone,
        otp : otp
    }
    return userModel.findOne(searchValue);
}



module.exports = {
    checkExistingUser : checkExistingUser,
    getUserDetails : getUserDetails
};