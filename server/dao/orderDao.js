'use strict'

const
    orderModel = require('../models/orderModel'),
    productModel = require('../models/productModel'),
    userModel = require('../models/userModel');
const { response } = require('express');
const { QueryInstance } = require('twilio/lib/rest/autopilot/v1/assistant/query');

async function placeOrder(orderDetails){
    orderDetails.orderDateTime = new Date();
    let orderObject = new orderModel(orderDetails);
    orderObject.save().then((data) => {
      
    return true;
    }).catch((err) => {

        return false;
    })
}

async function updateProductQuantity(productId, quantity){
    let searchObject = {
        id : productId
    }
    productModel.findOneAndUpdate(searchObject, {$inc : {
        quantity : -(quantity)
    }}).then(() => {
        return true;
    }).catch(() =>{
        return false;
    })
}

async function updateUserOrders(userId,quantity,price , orderId){
    let searchObject = {
        phone : userId
    };
    let pushObj = {
        orderId : orderId,
        orderDateTime : (new Date()),
        price : price,
        quantity : quantity
    }
    await userModel.findOneAndUpdate(searchObject, {$push : {
        orders : pushObj
    }}),async(err,res) => {
        return  true;
    }
}

async function getPreviousOrders(){
    let currentDate = new Date();
    let searchStartDate = new Date(currentDate.getDate()-3);
    currentDate.setHours(23,59,59);
    searchStartDate.setHours(0,0,0);
    let responseValue = await orderModel.find({
        orderDateTime : {
            $gte : searchStartDate,
            $lte : currentDate
        }
    }).sort({
        orderDateTime : 'desc'
    });
    return responseValue;
}
module.exports = {
    placeOrder : placeOrder,
    updateProductQuantity : updateProductQuantity,
    updateUserOrders : updateUserOrders,
    getPreviousOrders: getPreviousOrders
}