'use strict'

const
    express = require('express'),
    partnerService = require('../services/partnerService');

let router = express.Router();

router.post('/completeOrder', partnerService.completeOrder);

module.exports = router