'use strict'

const
    express = require('express'),
    cartService = require('../services/cartService');

let router = express.Router();

router.post('/addProduct', cartService.addProductToCart);
router.post('/remove', cartService.removeProductFromCart);
router.post('/getCart', cartService.getProductsInCart)
module.exports = router;