'use strict'

const
    express = require('express'),
    adminService = require('../services/adminService');

let router = express.Router();

router.post('/addProduct', adminService.addProduct);
router.post('/deleteProduct', adminService.deleteProduct);
router.post('/updateProduct', adminService.updateProduct)
module.exports = router;