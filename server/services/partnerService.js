'use strict'

const
    dao = require('../dao/partnerDao');

async function completeOrder(req,res){
    dao.completeOrder(req.body.orderId).then(()=>{
        res.json({
            error : false,
            status : "status updated successfully"
        })
    }).catch(() => {
        res.json({
            error : true,
            status : "error occured"
        })
    })
}

module.exports = {
    completeOrder : completeOrder
}