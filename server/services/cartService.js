'use strict'

const
    dao = require('../dao/cartDao')

async function addProductToCart(req, res){
    let isUpdateSuccess = false;
    await Promise.all( [dao.addProductToCart(req.body.productId, req.body.quantity, req.body.phone)]).then((data) =>{
       isUpdateSuccess = data[0];
    }).catch(() => {
        isUpdateSuccess = false;
    })
    if(isUpdateSuccess === true){
        res.json({
            error : false,
            message : 'cart updated successfully'
        });
    }else{
            res.json({
             error : true,
             message : ' cart update unsuccessful'
            })
    }
    // await Promise.all([
    //     dao.addProductToCart(req.body.productId, req.body.quantity, req.body.phone)
    // ]).then(() =)
}

async function getProductsInCart(req, res){
    var productsInCart;
    await Promise.all([
        dao.getProductsInCart(req.body.phone),
        dao.randomHealthFunction()
    ]).then((results) => {
        productsInCart = results[0];
        res.json(productsInCart);
    }).catch((err) => {
        res.json({
            message : "Failed to fetch objects",
            error : true
        })
    })
    
}
async function removeProductFromCart(req,res){
    let isProductRemoved = false;
    await Promise.all([
        dao.removeProductFromCart(req.body.phone, req.body.productId)
    ]).then((data) => {
        isProductRemoved = data[0]
    }).catch(() =>{
        isProductRemoved = false
    });
    if(isProductRemoved === true){
        res.json({
            error : false,
            message : 'cart updated successfully'
        });
    }else{
            res.json({
             error : true,
             message : ' cart update unsuccessful'
            })
    }
}

module.exports = { 
    addProductToCart : addProductToCart,
    getProductsInCart : getProductsInCart,
    removeProductFromCart : removeProductFromCart
}