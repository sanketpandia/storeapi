'use strict'

const
    dao = require('../dao/orderDao'),
    uuid = require('uuid-random');

async function placeOrder(req,res){
    let orderRequest = req.body.orders;
    let isUserDataUpdated = false, isOrderPlaced = false, isProductQuantityUpdated = false;
    orderRequest.forEach(async element => {
        
  
    {
        let orderId = uuid();
   let orderObject = {
        id : orderId,
        productId : element.productId,
        userId : element.userId,
        quantity : element.quantity,
        price : element.price,
        pendingStatus : true,
        addressLine1 : element.addressLine1,
        addressLine2 : element.addressLine2, 
        pincode : element.pincode, 
        city : element.city, 
        state : element.state
    }
    isUserDataUpdated = false, isOrderPlaced = false, isProductQuantityUpdated = false;

    await Promise.all([
        dao.placeOrder(orderObject),
        dao.updateProductQuantity(element.productId, element.quantity),
        dao.updateUserOrders(element.userId,element.quantity,element.price,orderId )
    ]).then((data) => {
        isOrderPlaced = data[0];
        isUserDataUpdated = data[2];
        isProductQuantityUpdated = data[1];
           
    })
    .catch(err => {
        res.json({
            error : true,
            message : "Placing order failed"    
        });
    });}

});
    if(isOrderPlaced && isProductQuantityUpdated && isUserDataUpdated){

        res.json({
            error : false,
            message : 'Order placing was successful'
        });
    }
    else{
        res.json({
            error : true,
            message : "Placing order failed"
        });
    }
}

/**
 * 
 * Function to fetch the orders of the previous 3 days
 * 
 */
async function fetchPreviousOrders(req, res){
    res.json(await dao.getPreviousOrders())
}

module.exports = {
    placeOrder : placeOrder,
    fetchPreviousOrders, fetchPreviousOrders
}