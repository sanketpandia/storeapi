const 
    dao = require('../dao/adminDao'),
    uuid = require('uuid-random');

async function addProduct(req, res){
    let productObj = {
        productId : uuid(),
        name : req.body.name,
        quantity : req.body.quantity,
        price : req.body.price,
        category : req.body.category,
        imageLink : req.body.imageLink,
        discount : req.body.discount
    }
    await dao.addProduct(productObj).then(() => {
        res.json({
            message : "Product inserted successfully"
        })
    }).catch(() => {
        res.json({
            message : "Product insertion unsuccessful"
        })
    })
}

async function updateProduct(req,res){
    let productObj = {
        productId : req.body.productId, 
        name : req.body.name,
        quantity : req.body.quantity,
        price : req.body.price,
        category : req.body.category,
        imageLink : req.body.image,
        discount : req.body.discount
    }
    await dao.editProduct(productObj).then(() => {
        res.json({
            message : "Product updated successfully"
        })
    }).catch(() => {
        res.json({
            message : "Product updation unsuccessful"
        })
    })
}

async function deleteProduct(req,res){
    await dao.deleteProduct(req.body.productId).then(() => {
        res.json({
            message : "Product deleted successfully"
        })
    }).catch(() => {
        res.json({
            message : "Product deletion unsuccessful"
        })
    }) 
}
module.exports = {
    addProduct : addProduct,
    updateProduct : updateProduct,
    deleteProduct : deleteProduct
}
