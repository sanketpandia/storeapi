'use strict'
const  
    mongoose = require('mongoose'),
    { Schema } = mongoose;

var orderSchema = new Schema({
    id : String,
    userId : String,
    productId : String,
    quantity : Number,
    price : Number,
    orderDateTime : Date,
    pendingStatus : Boolean,
    addressLine1 : String,
    addressLine2 : String, 
    pincode : Number, 
    city : String, 
    state : String
})

const order = mongoose.model('order',orderSchema);
module.exports = order;