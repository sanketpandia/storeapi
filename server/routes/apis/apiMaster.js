'use strict';

const
    express = require('express'),
    loginController = require('../../controllers/loginController'),
    searchController = require('../../controllers/searchController'),
    orderController = require('../../controllers/orderController'),
    partnerLoginController = require('../../controllers/partnerLoginController'),
    cartController = require('../../controllers/cartController'),
    partnerController = require('../../controllers/partnerController'),
    adminController = require('../../controllers/adminController'); 

let router = express.Router();

router.use('/login', loginController);
router.use('/search', searchController);
router.use('/order', orderController);
router.use('/partnerLogin', partnerLoginController);
router.use('/cart', cartController );
router.use('/partner' , partnerController);
router.use('/admin', adminController)
module.exports = router;